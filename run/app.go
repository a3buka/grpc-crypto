package run

import (
	"context"
	"fmt"
	"github.com/go-chi/chi/v5"
	jsoniter "github.com/json-iterator/go"
	"github.com/ptflp/godecoder"
	"gitlab.com/a3buka/grpc-crypto/config"
	"gitlab.com/a3buka/grpc-crypto/internal/db"
	"gitlab.com/a3buka/grpc-crypto/internal/infastacrute/components"
	"gitlab.com/a3buka/grpc-crypto/internal/infastacrute/db/migrate"
	"gitlab.com/a3buka/grpc-crypto/internal/infastacrute/db/scanner"
	"gitlab.com/a3buka/grpc-crypto/internal/infastacrute/errors"
	modules "gitlab.com/a3buka/grpc-crypto/internal/infastacrute/modules/worker"
	"gitlab.com/a3buka/grpc-crypto/internal/infastacrute/responder"
	"gitlab.com/a3buka/grpc-crypto/internal/infastacrute/router"
	"gitlab.com/a3buka/grpc-crypto/internal/infastacrute/server"
	model "gitlab.com/a3buka/grpc-crypto/internal/models/worker"
	"gitlab.com/a3buka/grpc-crypto/internal/storages"
	ggrpc "gitlab.com/a3buka/grpc-crypto/rpc/grpc"
	"gitlab.com/a3buka/grpc-crypto/rpc/grpc/proto"
	"gitlab.com/a3buka/grpc-crypto/rpc/jrpc"
	"gitlab.com/a3buka/grpc-crypto/worker"
	"go.uber.org/zap"
	"golang.org/x/sync/errgroup"
	"google.golang.org/grpc"
	"net/http"
	"net/rpc"
	"os"
)

// Application - интерфейс приложения
type Application interface {
	Runner
	Bootstraper
}

// Runner - интерфейс запуска приложения
type Runner interface {
	Run() int
}

// Bootstraper - интерфейс инициализации приложения
type Bootstraper interface {
	Bootstrap(options ...interface{}) Runner
}

// App - структура приложения
type App struct {
	conf     config.AppConf
	logger   *zap.Logger
	srv      server.Server
	jsonRPC  server.Server
	jsonGRPC server.Server
	Sig      chan os.Signal
	Storages *storages.Storages
	Servises *modules.Services
	Worker   worker.Worker
}

// NewApp - конструктор приложения
func NewApp(conf config.AppConf, logger *zap.Logger) *App {
	return &App{conf: conf, logger: logger, Sig: make(chan os.Signal, 1)}
}

// Run - запуск приложения
func (a *App) Run() int {
	// на русском
	// создаем контекст для graceful shutdown
	ctx, cancel := context.WithCancel(context.Background())

	errGroup, ctx := errgroup.WithContext(ctx)

	// запускаем горутину для graceful shutdown
	// при получении сигнала SIGINT
	// вызываем cancel для контекста
	errGroup.Go(func() error {
		sigInt := <-a.Sig
		a.logger.Info("signal interrupt recieved", zap.Stringer("os_signal", sigInt))
		cancel()
		return nil
	})

	// запускаем http сервер
	errGroup.Go(func() error {
		err := a.srv.Serve(ctx)
		if err != nil && err != http.ErrServerClosed {
			a.logger.Error("api: server error", zap.Error(err))
			return err
		}
		return nil
	})

	//запускаем json rpc сервер
	switch a.conf.RPCServer.Type {
	case "jrpc":
		errGroup.Go(func() error {
			err := a.jsonRPC.Serve(ctx)
			if err != nil {
				a.logger.Error("api: server error", zap.Error(err))
				return err
			}
			return nil
		})
	case "grpc":
		errGroup.Go(func() error {
			err := a.jsonGRPC.Serve(ctx)
			if err != nil {
				a.logger.Error("api: server error", zap.Error(err))
				return err
			}
			return nil
		})
	}
	//запускаем воркера
	a.Worker.WorkerRun()

	if err := errGroup.Wait(); err != nil {
		return errors.GeneralError
	}

	return errors.NoError
}

func (a *App) Bootstrap(options ...interface{}) Runner {
	decoder := godecoder.NewDecoder(jsoniter.Config{
		EscapeHTML:             true,
		SortMapKeys:            true,
		ValidateJsonRawMessage: true,
		DisallowUnknownFields:  true,
	})
	responseManager := responder.NewResponder(decoder, a.logger)

	components := components.NewComponents(a.conf, responseManager, decoder, a.logger)
	tableScanner := scanner.NewTableScanner()
	//дописать скан
	tableScanner.RegisterTable(
		&model.WorkerDTO{},
		&model.WorkerMaxMinAvgoDto{},
		&model.SellDto{},
	)
	dbx, sqlAdapter, err := db.NewSqlDB(a.conf.DB, tableScanner, a.logger)
	if err != nil {
		a.logger.Fatal("error init db", zap.Error(err))
	}
	migrator := migrate.NewMigrator(dbx, a.conf.DB, tableScanner)
	err = migrator.Migrate()
	if err != nil {
		a.logger.Fatal("migrator err", zap.Error(err))
	}
	newStorages := storages.NewStorage(sqlAdapter)
	a.Storages = newStorages

	services := modules.NewServices(newStorages, components)
	a.Servises = services
	controllers := modules.NewControllers(services, components)

	switch a.conf.RPCServer.Type {
	case "jrpc":
		workerRPC := jrpc.NewWorkerServerJSONRPC(services.Worker)
		jsonRPCServer := rpc.NewServer()
		err = jsonRPCServer.Register(workerRPC)
		if err != nil {
			a.logger.Fatal("error init auth json RPC", zap.Error(err))
		}
		a.jsonRPC = server.NewJSONRPC(a.conf.RPCServer, jsonRPCServer, a.logger)
	case "grpc":
		workerRPC := ggrpc.NewWorkerServiceJSONGRPC(a.Servises.Worker)
		jsonGRPCServer := grpc.NewServer()
		proto.RegisterExchangeServiceRPCServer(jsonGRPCServer, workerRPC)
		a.jsonGRPC = server.NewGRPCServer(a.conf.RPCServer, jsonGRPCServer, a.logger)
	default:
		a.logger.Info("RPC server is not started")
	}

	var r *chi.Mux
	r = router.NewRouter(controllers, components)
	srv := &http.Server{
		Addr:    fmt.Sprintf(":%s", a.conf.Server.Port),
		Handler: r,
	}
	// инициализация сервера
	a.srv = server.NewHttpServer(a.conf.Server, srv, a.logger)
	// инициализация воркера
	a.Worker = worker.NewWorkerRun(a.logger, a.Servises.Worker)
	// возвращаем приложение
	return a

}
