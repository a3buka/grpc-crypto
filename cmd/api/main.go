package main

import (
	"github.com/joho/godotenv"
	"gitlab.com/a3buka/grpc-crypto/config"
	"gitlab.com/a3buka/grpc-crypto/internal/infastacrute/logs"
	"gitlab.com/a3buka/grpc-crypto/run"
	"os"
)

func main() {
	err := godotenv.Load()
	config := config.NewAppConf()
	logger := logs.NewLogger(config, os.Stdout)
	if err != nil {
		logger.Fatal("error loading .env file")
	}
	config.Init(logger)
	app := run.NewApp(config, logger)
	exitCode := app.
		// Инициализируем приложение
		Bootstrap().
		// Запускаем приложение
		Run()
	os.Exit(exitCode)

}
