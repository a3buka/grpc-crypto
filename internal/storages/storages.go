package storages

import (
	"gitlab.com/a3buka/grpc-crypto/internal/db/adapter"
	"gitlab.com/a3buka/grpc-crypto/internal/infastacrute/modules/worker/storage"
)

type Storages struct {
	Worker storage.Workerer
}

func NewStorage(sqlAdapter *adapter.SQLAdapter) *Storages {
	return &Storages{Worker: storage.NewWorkerStorage(sqlAdapter)}
}
