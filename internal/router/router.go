package router

import (
	"github.com/go-chi/chi/v5"
	"gitlab.com/a3buka/grpc-crypto/internal/infastacrute/components"
	modules "gitlab.com/a3buka/grpc-crypto/internal/infastacrute/modules/worker"
	"net/http"
)

func NewApiRouter(controllers *modules.Controllers, components *components.Components) http.Handler {
	r := chi.NewRouter()

	return r
}
