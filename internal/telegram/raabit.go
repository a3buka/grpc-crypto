package telegram

import (
	"encoding/json"
	"github.com/rabbitmq/amqp091-go"
	model "gitlab.com/a3buka/grpc-crypto/internal/models/worker"
	"golang.org/x/net/context"
	"log"
)

func RabbitStart(dto model.SellDto) {
	conn, err := amqp091.Dial("amqp://guest:guest@rabitwork:5672/") //localhost
	if err != nil {
		panic(err)
		//logger.Error("не удалось установить соединение с RabbitMq", zap.Error(err))
	}
	ch, err := conn.Channel()
	if err != nil {
		panic(err)
	}
	defer ch.Close()

	q, err := ch.QueueDeclare(
		"add", // name
		true,  // durable
		false, // delete when unused
		false, // exclusive
		false, // no-wait
		nil,   // arguments
	)
	// структуру в байты
	body, _ := json.Marshal(dto)
	err = ch.PublishWithContext(
		context.Background(),
		"",
		q.Name,
		false,
		false,
		amqp091.Publishing{ContentType: "application/json", Body: body})
	if err != nil {
		log.Fatal(err)
	}
}
