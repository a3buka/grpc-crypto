package controller

import (
	"gitlab.com/a3buka/grpc-crypto/internal/models/worker"
)

//go:generate easytags $GOFILE
type RegisterRequest struct {
	Email          string `json:"email"`
	Password       string `json:"password"`
	RetypePassword string `json:"retype_password"`
	IdempotencyKey string `json:"idempotency_key"`
}

type ProfileResponseMin struct {
	Success   bool    `json:"success"`
	ErrorCode int     `json:"error_code,omitempty"`
	Data      DataMin `json:"data"`
}

type DataMin struct {
	Message string                 `json:"message,omitempty"`
	Worker  []model.WorkerMinPrice `json:"worker,omitempty"`
}

type ProfileResponseMax struct {
	Success   bool    `json:"success"`
	ErrorCode int     `json:"error_code,omitempty"`
	Data      DataMax `json:"data"`
}

type DataMax struct {
	Message string                 `json:"message,omitempty"`
	Worker  []model.WorkerMaxPrice `json:"worker,omitempty"`
}

type ProfileResponseAvg struct {
	Success   bool    `json:"success"`
	ErrorCode int     `json:"error_code,omitempty"`
	Data      DataAvg `json:"data"`
}

type DataAvg struct {
	Message string                 `json:"message,omitempty"`
	Worker  []model.WorkerAvgPrice `json:"worker,omitempty"`
}

type ProfileResponseHistory struct {
	Success   bool        `json:"success"`
	ErrorCode int         `json:"error_code,omitempty"`
	Data      DataHistory `json:"data"`
}

type DataHistory struct {
	Message string            `json:"message,omitempty"`
	Worker  []model.WorkerDTO `json:"worker,omitempty"`
}

type ChangePasswordRequest struct {
	OldPassword string `json:"old_password"`
	NewPassword string `json:"new_password"`
}

type ChangePasswordResponse struct {
	Success   bool   `json:"success"`
	ErrorCode int    `json:"error_code,omitempty"`
	Message   string `json:"message"`
}
