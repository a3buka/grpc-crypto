package service

import (
	"context"
	"fmt"
	"gitlab.com/a3buka/grpc-crypto/config"
	"gitlab.com/a3buka/grpc-crypto/internal/infastacrute/components"
	"gitlab.com/a3buka/grpc-crypto/internal/infastacrute/errors"
	"gitlab.com/a3buka/grpc-crypto/internal/infastacrute/modules/worker/storage"
	model "gitlab.com/a3buka/grpc-crypto/internal/models/worker"
	"go.uber.org/zap"
	"log"
)

type WorkerService struct {
	conf    config.AppConf
	storage storage.Workerer
	logger  *zap.Logger
}

func (w *WorkerService) SellDto(model model.SellDto) {
	err := w.storage.Sell(context.Background(), model)
	if err != nil {
		log.Fatal(err)
	}
}

func (w *WorkerService) MinPrices(ctx context.Context) WorkerMinPriceOut {
	minDto, err := w.storage.MinPrice(ctx)
	if err != nil {
		w.logger.Error("minPrice err", zap.Error(err))
		return WorkerMinPriceOut{
			ErrorCode: 2000,
			Succses:   false,
		}
	}
	return WorkerMinPriceOut{Worker: minDto, ErrorCode: errors.NoError, Succses: true}
}

func (w *WorkerService) MaxPrices(ctx context.Context) WorkerMaxPriceOut {
	maxDto, err := w.storage.MaxPrice(ctx)
	if err != nil {
		w.logger.Error("maxPrice err", zap.Error(err))
		return WorkerMaxPriceOut{
			ErrorCode: 2000,
			Succses:   false,
		}
	}
	return WorkerMaxPriceOut{Worker: maxDto, ErrorCode: errors.NoError, Succses: true}
}

func (w *WorkerService) AvgPrices(ctx context.Context) WorkerAvgPriceOut {
	avgDto, err := w.storage.AvgPrice(ctx)
	if err != nil {
		w.logger.Error("avgPrice err", zap.Error(err))
		return WorkerAvgPriceOut{
			ErrorCode: 2000,
			Succses:   false,
		}
	}
	return WorkerAvgPriceOut{Worker: avgDto, ErrorCode: errors.NoError, Succses: true}
}

func (w *WorkerService) History(ctx context.Context) WorkerHistory {
	history, err := w.storage.History(ctx)
	if err != nil {
		w.logger.Error("History", zap.Error(err))
		return WorkerHistory{
			ErrorCode: 2000,
			Succses:   false,
		}
	}
	return WorkerHistory{History: history, ErrorCode: errors.NoError, Succses: true}
}

func (w *WorkerService) MaxMinAvg(model model.WorkerMaxMinAvgoDto) {
	i, err := w.storage.MaxMinAvg(context.Background(), model)
	if err != nil {
		fmt.Println(err, i)
	}
}

func (w *WorkerService) TickerWork(model model.WorkerDTO) {
	i, err := w.storage.TickerWork(context.Background(), model)
	if err != nil {
		fmt.Println(err, i)
	}

}

func NewWorkerService(storage storage.Workerer, logger *zap.Logger, components *components.Components) *WorkerService {
	return &WorkerService{conf: components.Conf, storage: storage, logger: logger}
}
