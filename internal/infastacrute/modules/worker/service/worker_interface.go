package service

import (
	"context"
	model "gitlab.com/a3buka/grpc-crypto/internal/models/worker"
)

type Workerer interface {
	TickerWork(model model.WorkerDTO)
	MaxMinAvg(model model.WorkerMaxMinAvgoDto)
	MinPrices(ctx context.Context) WorkerMinPriceOut
	MaxPrices(ctx context.Context) WorkerMaxPriceOut
	AvgPrices(ctx context.Context) WorkerAvgPriceOut
	History(ctx context.Context) WorkerHistory
	SellDto(model model.SellDto)
}

type WorkerMinPriceOut struct {
	Worker    []model.WorkerMinPrice
	ErrorCode int
	Succses   bool
}

type WorkerMaxPriceOut struct {
	Worker    []model.WorkerMaxPrice
	ErrorCode int
	Succses   bool
}

type WorkerAvgPriceOut struct {
	Worker    []model.WorkerAvgPrice
	ErrorCode int
	Succses   bool
}

type WorkerHistory struct {
	History   []model.WorkerDTO
	ErrorCode int
	Succses   bool
}
