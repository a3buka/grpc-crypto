package storage

import (
	"context"
	"gitlab.com/a3buka/grpc-crypto/internal/telegram"
	"strconv"

	sq "github.com/Masterminds/squirrel"
	"gitlab.com/a3buka/grpc-crypto/internal/db/adapter"
	"gitlab.com/a3buka/grpc-crypto/internal/infastacrute/db/scanner"
	"gitlab.com/a3buka/grpc-crypto/internal/models/worker"
)

type WorkerStorage struct {
	adapter *adapter.SQLAdapter
}

func (s *WorkerStorage) Sell(ctx context.Context, models model.SellDto) error {
	dto := []model.SellDto{}
	search := []model.SellDto{}
	_ = s.adapter.List(ctx, &dto, models.TableName(), adapter.Condition{Equal: sq.Eq{
		"name": models.Name,
	}})
	if len(dto) < 1 {
		err := s.adapter.Create(ctx, &models)
		if err != nil {
			return err
		}
	} else {
		_ = s.adapter.List(ctx, &search, models.TableName(), adapter.Condition{Equal: sq.Eq{
			"name": models.Name,
		}})
		intSearch, _ := strconv.Atoi(search[0].SellPrice)
		intModel, _ := strconv.Atoi(models.SellPrice)
		if intSearch-intModel > 100 || intSearch-intModel <= -100 {
			telegram.RabbitStart(search[0])
			err := s.adapter.Update(ctx, &models, adapter.Condition{
				Equal: sq.Eq{
					"name": models.Name,
				},
			}, scanner.Update)
			if err != nil {
				return err
			}
		}
	}
	return nil
}

func (s *WorkerStorage) MaxPrice(ctx context.Context) ([]model.WorkerMaxPrice, error) {
	dto := []model.WorkerMaxPrice{}
	err := s.adapter.ListFields(ctx, &dto, "WorkerMaxMinAvgoDto", adapter.Condition{}, "name", "high")
	if err != nil {
		return []model.WorkerMaxPrice{}, err
	}
	return dto, nil
}

func (s *WorkerStorage) AvgPrice(ctx context.Context) ([]model.WorkerAvgPrice, error) {
	dto := []model.WorkerAvgPrice{}
	err := s.adapter.ListFields(ctx, &dto, "WorkerMaxMinAvgoDto", adapter.Condition{}, "name", "avg")
	if err != nil {
		return []model.WorkerAvgPrice{}, err
	}
	return dto, nil
}

func (s *WorkerStorage) MinPrice(ctx context.Context) ([]model.WorkerMinPrice, error) {
	dto := []model.WorkerMinPrice{}
	err := s.adapter.ListFields(ctx, &dto, "WorkerMaxMinAvgoDto", adapter.Condition{}, "name", "low")
	if err != nil {
		return []model.WorkerMinPrice{}, err
	}
	return dto, nil
}

func (s *WorkerStorage) MaxMinAvg(ctx context.Context, models model.WorkerMaxMinAvgoDto) (int, error) {
	dto := []model.WorkerMaxMinAvgoDto{}
	err := s.adapter.List(ctx, &dto, models.TableName(), adapter.Condition{Equal: sq.Eq{
		"name": models.Name,
	}})
	if len(dto) < 1 {
		err := s.adapter.Create(ctx, &models)
		if err != nil {
			return 0, err
		}
	} else {
		err := s.adapter.Update(ctx, &models, adapter.Condition{
			Equal: sq.Eq{
				"name": models.Name,
			},
		}, scanner.Update)
		if err != nil {
			return 0, err
		}
	}
	return 1, err
}

func (s *WorkerStorage) History(ctx context.Context) ([]model.WorkerDTO, error) {
	dto := []model.WorkerDTO{}
	err := s.adapter.List(ctx, &dto, "workerDTO", adapter.Condition{})
	if err != nil {
		return []model.WorkerDTO{}, err
	}
	return dto, nil
}

func (s *WorkerStorage) TickerWork(ctx context.Context, model model.WorkerDTO) (int, error) {
	err := s.adapter.Create(ctx, &model)
	return 0, err
}

func NewWorkerStorage(sqlAdapter *adapter.SQLAdapter) Workerer {
	return &WorkerStorage{adapter: sqlAdapter}
}

func (s *WorkerStorage) Create(ctx context.Context, u model.WorkerDTO) (int, error) {
	err := s.adapter.Create(ctx, &u)
	return 0, err
}
