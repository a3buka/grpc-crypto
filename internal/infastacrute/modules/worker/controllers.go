package modules

import (
	"gitlab.com/a3buka/grpc-crypto/internal/infastacrute/components"
	"gitlab.com/a3buka/grpc-crypto/internal/infastacrute/modules/worker/controller"
)

type Controllers struct {
	Worker controller.Workerer
}

func NewControllers(services *Services, components *components.Components) *Controllers {

	workerController := controller.NewWorker(services.Worker, components)

	return &Controllers{
		Worker: workerController,
	}
}
