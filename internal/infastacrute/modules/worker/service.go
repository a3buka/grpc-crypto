package modules

import (
	"gitlab.com/a3buka/grpc-crypto/internal/infastacrute/components"
	"gitlab.com/a3buka/grpc-crypto/internal/infastacrute/modules/worker/service"
	"gitlab.com/a3buka/grpc-crypto/internal/storages"
)

type Services struct {
	Worker service.Workerer
}

func NewServices(storages *storages.Storages, components *components.Components) *Services {
	return &Services{

		Worker: service.NewWorkerService(storages.Worker, components.Logger, components),
	}
}
